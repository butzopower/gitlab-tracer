# GitLab Tracer Project

The purpose of this project is to verify expected GitLab CI/CD functionality on GitLab, as well as act as a very basic example node project.

## Tests

There's a very basic test that checks it is possible to store and retrieve a value in redis.  On GitLab CI, this test should run against a spun up Redis service via GitLab pipelines.

## Deployment

If the tests all pass, the GitLab pipeline should then attempt to deploy the very basic hello-world node app to PCF.  The following [environment variables need to be configured](https://docs.gitlab.com/ee/ci/variables/README.html#gitlab-cicd-environment-variables) in the CI/CD settings section on GitLab in order for it to work.

Required environment variables:

- `CF_API` - the host of your PCF API 
- `CF_USER` - the login user for PCF (ideally mark as masked in GitLab)
- `CF_PASSWORD` - the password for the login user for PCF (ideally mark as masked in GitLab)
- `CF_ORG` - the CF org you wish to deploy to
- `CF_SPACE` - the CF space you with the app to run in
